#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import shutil
import random
import datetime
import tempfile
import subprocess
import functools

from flask import Flask, request, session, render_template, redirect, \
    url_for, abort, flash, make_response, g
import sqlite3

import opinion


app = Flask(__name__)

app.config.update(dict(
    DATABASE='./db.db',
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def init_db():
    """Creates the database tables."""
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        with app.open_resource('triggers.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        with app.open_resource('sample.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()


def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


@app.errorhandler(404)
def page_not_found(error):
    return redirect(url_for('inbox'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        pass_matches = query_db(
            'select pesel, haslo from pracownik\n'
            'where pesel = ? and haslo = ?',
            (request.form['login'], request.form['password']))
        if pass_matches:
            session['logged_in'] = True
            session['login'] = request.form['login']
            session['name'] = get_name(session['login'], space=' ')
            return redirect(url_for('inbox'))
        else:
            # wyświetl informację o błędnych danych logowania
            flash('Podano błędny pesel lub hasło')
    # wyświetl błąd, gdy błędne hasło
    return render_template('login.html')


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    return redirect(url_for('login'))


def logged_in(func):
    "Make sure if user is logged in."
    @functools.wraps(func)
    def wrapper(*args, **kwds):
        if not 'logged_in' in session:
            return redirect(url_for('login'))
            #abort(401)
        return func(*args, **kwds)
    return wrapper


def count_no(func):
    @functools.wraps(func)
    def wrapper(*args, **kwds):
        pesel = session['login']

        # obliczenie liczby powiadomień
        notification_no_row = query_db(
            'select count(*) as count from zgloszenie '
            'where pesel_zglaszajacego = ? '
            'and id_opinii is not NULL',
            [pesel],
            True)
        notification_no = notification_no_row['count']
        session['notification_no'] = notification_no

        # obliczenie liczby zgłoszeń
        request_no_row = query_db(
            'select count(*) as count from zgloszenie '
            'where pesel_wystawiajacego = ? '
            'and id_opinii is NULL',
            [pesel],
            True)
        request_no = request_no_row['count']
        session['request_no'] = request_no
        return func(*args, **kwds)
    return wrapper


@app.route('/')
@logged_in
@count_no
def inbox():
    return render_template('inbox.html')


@app.route('/fill-cert', methods=['GET', 'POST'])
@logged_in
@count_no
def fill_cert():
    if request.method == 'POST':
        req_pesel = session['login']
        filler_pesel = request.form['pesel']
        commentary = request.form['text']
        get_db().execute(
            'insert into zgloszenie '
            '(pesel_zglaszajacego, pesel_wystawiajacego, komentarz) '
            'values(?, ?, ?)',
            [req_pesel, filler_pesel, commentary])
        get_db().commit()
        flash('Zaświadczenie odłożone do generacji')
    workers = query_db(
        'select pesel, imie, nazwisko from Pracownik where pesel != ?',
        [session['login']])
    return render_template('creator.html', workers=workers)


@app.route('/certs')
@logged_in
@count_no
def show_certs():
    pesel = session['login']
    certs = query_db(
        'select id, data_wystawienia, opinia from zaswiadczenie '
        'where pesel = ?',
        [pesel])
    return render_template('certs.html', certs=certs)


@app.route('/certs/<int:cert_id>')
@logged_in
@count_no
def download_cert(cert_id):
    # FIXME: usuwanie zgłoszenia przy wyświetlaniu opinii...
    get_db().execute(
        'delete from zgloszenie where id_opinii = ?',
        [cert_id])
    get_db().commit()
    path_row = query_db(
        'select sciezka from zaswiadczenie '
        'where id = ?',
        [cert_id], True)
    path = path_row['sciezka']
    cert = open(path, mode='rb')
    response = make_response(cert.read())
    response.content_type = 'application/pdf'
    return response


@app.route('/notifications')
@logged_in
@count_no
def show_notifications():
    pesel = session['login']
    new_opinions = query_db(
        'select zaswiadczenie.id, data_wystawienia, opinia from '
        'zaswiadczenie join zgloszenie on '
        'zaswiadczenie.id = zgloszenie.id_opinii '
        'where pesel = ?',
        [pesel])
    return render_template('certs.html', certs=new_opinions)


@app.route('/pending-opinions')
@logged_in
@count_no
def show_pending_opinions():
    pesel = session['login']
    opinions = query_db(
        'select id, data_zgloszenia, imie, nazwisko, komentarz from '
        'zgloszenie, pracownik '
        'where pracownik.pesel = zgloszenie.pesel_zglaszajacego and '
        'pesel_wystawiajacego = ? and id_opinii is NULL',
        [pesel])
    return render_template('opinions.html', opinions=opinions)


def generate_opinion_path():
    # FIXME: jest taka zmienna w biblotece...
    chars = "abcdefghijklmnoprstuwxyz"
    length = 15
    filename = ""
    for _ in range(length):
        filename += random.choice(chars)
    filename += '.pdf'
    path = os.path.join('./certs', filename)
    if os.path.exists(path):
        return generate_opinion_path()
    return path


def generate_pdf(tex):
    f = tempfile.NamedTemporaryFile(suffix='.tex', delete=False)
    f.write(tex.encode('utf-8'))
    tex_path = f.name
    f.close()
    subprocess.call(['pdflatex', '-output-directory', r'/tmp', tex_path])
    path, ext = os.path.splitext(tex_path)
    out_path = path + '.pdf'
    return out_path


def get_name(pesel, space='~'):
    row = query_db(
        'select imie, nazwisko from pracownik '
        'where pesel = ?',
        [pesel], True)
    name = row['imie'] + space + row['nazwisko']
    return name


@app.route('/pending-opinions/<int:request_id>', methods=['GET', 'POST'])
@logged_in
@count_no
def fill_opinion(request_id):
    if request.method == 'POST':
        filler_pesel = session['login']
        text = request.form['text'].strip()
        receiver_row = query_db(
            'select pesel_zglaszajacego from zgloszenie '
            'where id = ?',
            [request_id], True)
        receiver_pesel = receiver_row['pesel_zglaszajacego']
        receiver_name = get_name(receiver_pesel)
        filler_name = get_name(filler_pesel)
        tex = opinion.template.format(
            filler=filler_name,
            receiver=receiver_name,
            date=datetime.datetime.now().strftime('%Y/%m/%d'),
            text=text)
        pdf_path = generate_pdf(tex)
        path = generate_opinion_path()
        shutil.move(pdf_path, path)
        get_db().execute(
            'insert into zaswiadczenie '
            '(sciezka, pesel, opinia, pesel_wydajacego) '
            'values(?, ?, ?, ?)',
            [path, receiver_pesel, text, filler_pesel])
        # id wygenerowanej opinii
        # FIXME: lipa
        opinion_id_row = query_db('select max(id) as id from zaswiadczenie',
                                  one=True)
        opinion_id = opinion_id_row['id']
        # Uzupełnij zgłoszenie o id_opinii, aby nie wyświetlać go ponownie jako
        # oczekującego na wystawienie.
        get_db().execute(
            'update zgloszenie '
            'set id_opinii = ? '
            'where id = ?',
            [opinion_id, request_id])
        get_db().commit()
        return redirect(url_for('show_pending_opinions'))
    name_row = query_db(
        'select imie, nazwisko from zgloszenie join pracownik '
        'on zgloszenie.pesel_zglaszajacego = pracownik.pesel '
        'where zgloszenie.id = ?',
        [request_id], True)
    return render_template('fill_opinion.html',
                           request_id=request_id, name=name_row)


if __name__ == '__main__':
    if not os.path.exists(app.config['DATABASE']):
        init_db()
    if not os.path.exists('certs'):
        os.mkdir('certs')
    app.run()
