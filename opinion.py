#!/usr/bin/env python
# -*- coding: utf-8 -*-


template = r'''
\documentclass[a4paper,12pt]{{article}}


\usepackage[MeX]{{polski}}
\usepackage[utf8]{{inputenc}}
%\usepackage{{indentfirst}}
% ew. utf8 lub cp1250
% Zdefiniowanie autora i~tytułu:
\author{{Wystawiona dla: {receiver} \\
		przez: {filler} }}
\title{{Opinia}}
\date{{ {date} }}
\frenchspacing
\begin{{document}}
\maketitle

{text}\\\\

\hfill Podpis wystawiającego:
\end {{document}}
'''
