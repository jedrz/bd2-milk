DROP TABLE IF EXISTS Pracownik;
DROP TABLE IF EXISTS Zaswiadczenie;
DROP TABLE IF EXISTS Zgloszenie;
    
CREATE TABLE Pracownik
  (
    pesel INTEGER  PRIMARY KEY ,
    imie TEXT NOT NULL ,
    nazwisko TEXT NOT NULL ,
    haslo TEXT NOT NULL
  ) ;

CREATE TABLE Zaswiadczenie
  (
    id               INTEGER PRIMARY KEY AUTOINCREMENT ,
    data_wystawienia TEXT ,
    sciezka TEXT NOT NULL ,
    pesel INTEGER  NOT NULL ,
    opinia TEXT ,
    pesel_wydajacego INTEGER,

    FOREIGN KEY(pesel) REFERENCES Pracownik(pesel),
    FOREIGN KEY(pesel_wydajacego) REFERENCES Pracownik(pesel)
  ) ;

CREATE TABLE Zgloszenie
  (
    id                  INTEGER PRIMARY KEY AUTOINCREMENT ,
    data_zgloszenia     TEXT ,
    pesel_zglaszajacego INTEGER  NOT NULL ,
    id_opinii           INTEGER ,
    pesel_wystawiajacego INTEGER  NOT NULL ,
    komentarz TEXT,
    FOREIGN KEY(pesel_zglaszajacego) REFERENCES Pracownik(pesel),
    FOREIGN KEY(pesel_wystawiajacego) REFERENCES Pracownik(pesel),
    FOREIGN KEY(id_opinii) REFERENCES Zaswiadczenie(id)
  );
